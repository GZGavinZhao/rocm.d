#!/bin/bash

set -oex pipefail

cc -D__HIP_PLATFORM_AMD__ -E /usr/include/hip/hip_runtime_api.h > source/hip_runtime_api.i

dstep -o hip_runtime_api.d hip_runtime_api.i
